#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time 
import datetime
import os

def writePointsToFile(path, sensorName, timestamp, values):
    d = datetime.datetime.fromtimestamp(timestamp/1000).strftime('%Y-%m-%d')
    fn = "{}/{}_{}.csv".format(path, sensorName, d)

    first = False
    if not os.path.exists(fn):
        first = True

    with open(fn, "a") as csv:
        if first:
            l = "timestamp, " + ", ".join(["\"[{}] {}\"".format(x[1], x[2]) for x in values])
            csv.write(l+"\n")
        l = "{}, ".format(timestamp) + ", ".join(["{}".format(x[3]) for x in values])
        csv.write(l+"\n")

