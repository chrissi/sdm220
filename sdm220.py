#!/usr/bin/env python
# -*- coding: utf-8 -*-

from CRC16 import CRC16
import serial
import time 
import struct
import datetime
import argparse
from write import writePointsToFile
from modbus import readInputRegister  

# SDM220 Modbus documentation can be found here:
# http://www.flanesi.it/blog/download/sdm220modbus/SDM220Modbus_protocol_V1.1.pdf

# Datasheet is here:
# http://bg-etech.de/download/manual/SDM220ModbusEN.pdf


parser = argparse.ArgumentParser(description="Log measurements from a SDM220 smartmeter to a csv-file")
parser.add_argument("intervall", type=float, help="sampling intervall in seconds")
parser.add_argument("sensorName",  help="name of the sensor for output")
parser.add_argument("outputPath",  help="existing path to output to")
parser.add_argument("tty",  help="serial device to open")
parser.add_argument("modbusAddr", type=int, help="modbus address of the device")

args = parser.parse_args()

ser = serial.Serial(port=args.tty, baudrate=9600, timeout=0.1)

crc = CRC16(modbus_flag = True)

offsets = [(0x00  , "V", "line to neutral voltage"), \
           (0x06/2, "A", "current"), \
           (0x0C/2, "W", "Active Power"), \
           (0x12/2, "VA", "Apparent Power"), \
           (0x18/2, "VAr", "Reactive Power"), \
           (0x1E/2, "None", "Power Factor"), \
           (0x24/2, "°", "Phase Angle"), \
           (0x46/2, "Hz","Frequency")]

while True:
    timestamp, r = readInputRegister(ser, 0x00, length=72, addr=args.modbusAddr, crc16=crc)
  
    if r != False:
        for x in offsets:
            print "{} {}".format(r[x[0]], x[1])
        out = [x + (r[x[0]],) for x in offsets]

        writePointsToFile(args.outputPath, args.sensorName, timestamp, out)
    time.sleep(args.intervall)
