Python tools to read Easton / BG-Etech SDM 220 Modbus power meter via RS485 and currently write the obtained data to csv-files.

This repo mainly consists of thee modules:

# Main Module:
Call this modules if you want to measure data from a power meter.
All modules use argparse, so just ask them what they need.

```$ ./sdm220.py -h
usage: sdm220.py [-h] intervall sensorName outputPath tty modbusAddr

Log measurements from a SDM220 smartmeter to a csv-file

positional arguments:
  intervall   sampling intervall in seconds
  sensorName  name of the sensor for output
  outputPath  existing path to output to
  tty         serial device to open
  modbusAddr  modbus address of the device

optional arguments:
  -h, --help  show this help message and exit```

Logging currently happens to csv only. But I guess tweaking this to your needs should be easy.



# Modbus-Module 
This module handles the Modbus-Communication. Only the needed Modbus-Function 0x04 readInputRegister is implemented.
This module is mainly straigt forward. See the Modbus-Documentation given as a link.

The only gimmick is used to handle 0x00-Bytes sometimes happening in front and after the PDU read from the master.
Leading 0x00-Bytes are skipped. This makes broadcast-request impossible - but that is ok for me.
Trailing 0x00-Bytes are a little more difficult. To handle them possile PDUs are extracted from the read PDU. For every possible PDU the CRC is calculated and compared to the one found in the PDU. If they match the real PDU is considered found. This should only fail in the magnitude of 1/2^32 what sounds ok to me.

The module generates a microsecond-timestamp at the point in time the request-PDU is send to the master.

# Write-Module 
This module writes lists of data to a csv-file. A file is created for every day. A microsecond-timestamp is used.
